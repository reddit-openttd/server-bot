import { onPrivateChat, onChat }  from '../services/rabbitmq'
import { sendPrivateMessage,  getClients, getCompanies, sendBroadcastMessage, sendRcon,  } from '../services/restmin'
import { Client, Company } from '../types/OpenTTDApi'
import { Chat, OpenTTDEvent } from '../types/OpenTTDEvent'

const handler = (event: OpenTTDEvent<Chat>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!reset' && command !== '!resetme') { return }

  return getClients(event.serverHost)
    .then((clients: Client[]) => {
      const requestClient = clients.find(c => c.id === chatDetail.clientId)
      
      if (!requestClient) {
        console.log(`BOT ERROR: Client ${chatDetail.clientId} not found!`)
        return sendPrivateMessage(event.serverHost, chatDetail.clientId, `Sorry! An internal logic error occured. Please notify the admins!`)
      }

      if (!requestClient.companyId || requestClient.companyId === 255) {
        if (!requestClient.companyId) {
          console.log(`User ${requestClient.name}'s company, ${requestClient.companyId}, is not well formed!`)
        }
        return sendPrivateMessage(event.serverHost, chatDetail.clientId, 'You can\'t dissolve the spectators, that\'d be silly!')
      }

      const otherCompanyClients = clients.filter(c => 
        c.companyId === requestClient.companyId && c.id !== requestClient.id)
      if (otherCompanyClients.length > 0) {
        return sendPrivateMessage(event.serverHost, chatDetail.clientId, `Your company has other players in it - get them to leave before resetting!`)
      }

      return getCompanies(event.serverHost)
        .then((companies: Company[]) => {
          const reqCompany = companies.find(c => c.id === requestClient.companyId)
          if (!reqCompany) {
            console.log(`BOT ERROR: Company ${requestClient.companyId} not found!`)
            return sendPrivateMessage(event.serverHost, chatDetail.clientId, `Sorry! An internal logic error occured. Please notify the admins!`)
          }
          console.log(`Resetting company ${reqCompany.id + 1} (${reqCompany.name})`)
          sendBroadcastMessage(event.serverHost, `*** ${requestClient.name} has reset their company (${reqCompany.name})`)
          return sendRcon(event.serverHost,`move ${requestClient.id} 255`)
            .then(() => sendRcon(event.serverHost,`reset_company ${reqCompany.id}`))
        })
    })
    .catch(err => {
      console.log(`Unhandled error trying to reset company: ` + err)
      console.error(err)
    })
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}
