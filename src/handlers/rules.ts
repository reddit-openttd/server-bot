import { onPrivateChat, onChat }  from '../services/rabbitmq'
import { sendPrivateMessage } from '../services/restmin'
import { Chat, OpenTTDEvent } from '../types/OpenTTDEvent'
import {curry} from 'ramda'
import {sendMultiline} from '../helper/sender'

const redditRules = `House rules:
1. Be nice & respectful to other players
2. No blocking other players
3. Do not steal non-primary resources
4. Do not abuse station spread to beam resources
These rules are only in place to support a healthy competitive atmosphere where everyone can have fun.
These rules are explained more in depth here: https://www.reddit.com/r/openttd/wiki/rules`

const jgr1Rules = `0: Admins judgment is final. 
1: Keep your in-game name and discord name the same. 
2: No complaining or being rude about in game settings or newgrf selection. If there's a crucial setting that got missed or a newgrf you'd like to request, please ask nicely in the requests forum and your request will be considered. Please respect admins choice on this.
3: Do not join un-passworded companies without permission from company owner.
4: No griefing, period. You will get IP banned so fast.
5: Infrastructure sharing is enabled. Don't connect to another person's network without permission.
6: Planning is how you select your claim. Use p to open the plans menu; valid claims come in the form of claiming areas, cities, industries and/or lines.
Do not overclaim (i.e. more than 1/15th of the playable area). Make sure to make your plans public. 
7: Do not use industries and cities outside of claims.
8: Terraforming: Don’t mass terraform in a way that looks unnatural, please be reasonable.
9: Respect other peoples play styles. Nobody plays exactly the same way, but be willing to learn if you want to actively cooperate with other players.`

const jgr2Rules = `0: All rules from server 01 apply here as well.
1: Please adhere to the guidelines in the provided tutorials. If you do not wish to conform to this playstyle - that’s totally okay - we have server 01 for you.
2: Your abilities will not exclude you from server 02. Just be ready to learn and be open to advice from more experienced players.
3: If you happen to be a more experienced player please be patient with noobies and help them out if they have questions NICELY.
4: Claiming a large important area of the map and leaving after a few days is very frowned upon in S2. 
5: Be willing to cooperate with your neighbors. We're not here to play single player.
6: Make acceptable aesthetic choices to fit in with the theme of the server.
  a) No one tile turns unless they’re hidden.
  b) Build rail lines around terrain if there’s an option and do not terraform in a way that looks excessive or unnatural.
  c) Build junctions and rail lines for appropriate traffic levels. Avoid building your networks around cloverleafs.
7: "Cheesing" the goal conditions will disqualify you from receiving any rewards for goal conditions. 
8: Try not to always overbuild, there doesn’t need to be a huge city in every region of the map. `

const jgr3Rules = `0: Admin's judgement is final.
1: Communication is the key to a pleasant game; keep your in-game name and discord name the same.
2: Do not join un-passworded companies without permission from company owner.
3: No griefing, period.
4: Infrastructure sharing is enabled on all servers. Don't connect to another person's network without permission.
A deeper explanation of these rules can be found in the Welcome channel in the discord.`

const rules: Record<string, string> = {
  'jgr1': jgr1Rules,
  'jgr2': jgr2Rules,
  'jgr3': jgr3Rules,
  'server1': redditRules,
  'reddit1': redditRules,
  'reddit2': redditRules,
  'reddit3': redditRules
}

const handler = (event: OpenTTDEvent<Chat>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!rules') { return }
  
  const serverRules = rules[event.serverName] || redditRules
  const send = curry(sendPrivateMessage)(event.serverHost, chatDetail.clientId)

  return sendMultiline(serverRules.split('\n'), send)
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}