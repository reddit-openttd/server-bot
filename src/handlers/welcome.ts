import { onJoin }  from '../services/rabbitmq'
import { sendPrivateMessage } from '../services/restmin'
import { ClientEvent, OpenTTDEvent } from '../types/OpenTTDEvent'
import {curry} from 'ramda'


export const init = () => {
  const defaultWelcome = process.env.OPENTTD_WELCOME
  if (!defaultWelcome) {
    return
  }
  const defaultWelcomeArray = defaultWelcome.split('\n')

  onJoin((event: OpenTTDEvent<ClientEvent>) => {
    let welcomeArray = defaultWelcomeArray

    if (event.serverName) {
      const envVar = `OPENTTD_${event.serverName.toUpperCase()}_WELCOME`
      if (process.env[envVar]) {
        welcomeArray = (process.env[envVar] || '').split('\n')
      }
    }

    const send = curry(sendPrivateMessage)(event.serverHost, event.data.clientId)
    
    return welcomeArray
      .filter(message => !!message)
      .reduce((promise, message) => promise.then(() => send(message)), Promise.resolve())
        .catch(err => {
          console.error('Error calling API: ' + err.message)
        })  
  })
}