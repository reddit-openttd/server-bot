import { sendMultiline } from '../helper/sender'
import { onPrivateChat, onChat }  from '../services/rabbitmq'
import { sendPrivateMessage } from '../services/restmin'
import { Chat, OpenTTDEvent } from '../types/OpenTTDEvent'
import {curry} from 'ramda'

const defaultHelp = [
  "!help - this menu",
  "!reset - remove your company",
  "!end - shows how much time left in the game",
  "!discord - link to the discord",
  "!admin <message> - request an admin from discord",
  "!goals - describes the goals for this server"
]
const jgrHelp = [
  "!help - this menu",
  "!reset - remove your company",
  "!admin <message> - request an admin from discord",
  "!goals - describes the goals for this server"
]

const help = {
  'jgr1': jgrHelp,
  'jgr2': jgrHelp,
  'jgr3': jgrHelp,
}

const handler = (event: OpenTTDEvent<Chat>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!help' && command !== '!commands') { return }
  
  const serverHelp = help[event.serverName] || defaultHelp

  const send = curry(sendPrivateMessage)(event.serverHost, chatDetail.clientId)

  return sendMultiline(serverHelp, send)
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}