
import { sendMultiline } from '../helper/sender'
import { onPrivateChat, onChat }  from '../services/rabbitmq'
import { sendPrivateMessage } from '../services/restmin'
import { Chat, OpenTTDEvent } from '../types/OpenTTDEvent'
import {curry} from 'ramda'

const defaultGoals = `This is a sandbox game, you make up your own goals.`

const goals = {
  'jgr2': `If you meet these goal conditions you will receive extra role points on the discord.
  There are player and cooperative goals. You receive +1 for meeting all goal conditions in each category.
  Anyone who contributes to the cooperative goals will receive +1 for them.
  Player goals for current game: +1
    1) Connect every town in your claim with passenger service.
    2) Grow one of your cities to 20k
    3) Connect your claim to your quadrants cooperative city with passenger service and make sure your cargo dist links are in the green by the end of the game.
  Cooperative goals for the current game: +1
    1) Grow your quadrants cooperative city to 100k.
    2) Send a rail line to the Stuttgart Administrative Zone and make sure the cargo dist links are in the green by the end of the game.
  There is also a BONUS goal for having the most complete and best looking claim. This will be judged by admins before the next reset. 
  Meeting your bonus goal will get you +2 points.`
}

const handler = (event: OpenTTDEvent<Chat>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!goals') { return }
  
  const serverGoals = goals[event.serverName] || defaultGoals
  const send = curry(sendPrivateMessage)(event.serverHost, chatDetail.clientId)

  return sendMultiline(serverGoals.split('\n'), send)
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}