import { onDate, onWelcome }  from '../services/rabbitmq'
import { getSetting, sendBroadcastMessage, setNewGameSetting, setSetting } from '../services/restmin'
import { HttpServerHost, OpenTTDEvent } from '../types/OpenTTDEvent'
import { GameInfo } from '../types/GameInfo'
import { getGameDurationMs} from '../helper/date'
import { Setting } from '../types/OpenTTDApi'

const MINS_TO_APPLY = 5

type EndGameInfo = {
  endYear: number,
  dayLengthFactor: number,
  appliedNextGameSettings: boolean
}

type RotationSetting = {
  name: string,
  settings: Setting[]
}

let endGameInfo: Record<string, EndGameInfo> = {}

const rotation: RotationSetting[] = [{
  name: 'Temperate',
  settings: [
    {section: 'game_creation',    setting: 'map_x',               value: '10'},
    {section: 'game_creation',    setting: 'map_y',               value: '9'},
    {section: 'game_creation',    setting: 'landscape',           value: 'temperate'},
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: '3'}, // Rough
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: '2'}, // Medium
    {section: 'difficulty',       setting: 'terrain_type',        value: '3'}, // Hilly
    {section: 'difficulty',       setting: 'number_towns',        value: '1'}, // Low
    {section: 'difficulty',       setting: 'industry_density',    value: '4'}, // normal
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'construction',     setting: 'extra_tree_placement', value: '0'} // none
  ]
}, {
  name: 'Arctic',
  settings: [
    {section: 'game_creation',    setting: 'map_x',               value: '10'},
    {section: 'game_creation',    setting: 'map_y',               value: '9'},
    {section: 'game_creation',    setting: 'landscape',           value: 'arctic'},
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: '2'}, // Smooth
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: '2'}, // Medium
    {section: 'difficulty',       setting: 'terrain_type',        value: '4'}, // Mountainous
    {section: 'difficulty',       setting: 'number_towns',        value: '1'}, // Low
    {section: 'difficulty',       setting: 'industry_density',    value: '3'}, // low
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'construction',     setting: 'extra_tree_placement', value: '0'} // none
  ]
}, {
  name: 'Tropical',
  settings: [
    {section: 'game_creation',    setting: 'map_x',               value: '10'},
    {section: 'game_creation',    setting: 'map_y',               value: '11'},
    {section: 'game_creation',    setting: 'landscape',           value: 'tropic'},
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: '2'}, // Smooth
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: '1'}, // Few
    {section: 'difficulty',       setting: 'terrain_type',        value: '3'}, // Hilly
    {section: 'difficulty',       setting: 'number_towns',        value: '4'}, // Custom
    {section: 'game_creation',    setting: 'custom_town_number',  value: '40'}, // Custom (VERY low)
    {section: 'difficulty',       setting: 'industry_density',    value: '3'}, // low
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'game_creation',    setting: 'desert_coverage',     value: '60'}, // 60%
    {section: 'construction',     setting: 'extra_tree_placement', value: '2'} // Normal
  ]
},{
  name: 'Toyland',
  settings: [
    {section: 'game_creation',    setting: 'map_x',               value: '9'},
    {section: 'game_creation',    setting: 'map_y',               value: '9'},
    {section: 'game_creation',    setting: 'landscape',           value: 'toyland'},
    {section: 'game_creation',    setting: 'tgen_smoothness',     value: '2'}, // Smooth
    {section: 'game_creation',    setting: 'amount_of_rivers',    value: '2'}, // Medium
    {section: 'difficulty',       setting: 'terrain_type',        value: '3'}, // Hilly
    {section: 'difficulty',       setting: 'number_towns',        value: '1'}, // Low
    {section: 'difficulty',       setting: 'industry_density',    value: '4'}, // normal
    {section: 'game_creation',    setting: 'water_borders',       value: '16'}, // Random
    {section: 'construction',     setting: 'extra_tree_placement', value: '0'} // none
  ]
}]

const getNextGameSettings = (serverHost: HttpServerHost): Promise<RotationSetting> => {
  return getSetting(serverHost, 'game_creation', 'landscape')
    .then(setting => {
      switch(setting.value) {
        case 'temperate': return rotation[1]
        case 'arctic': return rotation[2]
        case 'tropic': return rotation[3]
        case 'toyland': return rotation[0]
        default: return rotation[0]
      }
    })
}

const getEndGameInfo = (host: HttpServerHost) => {
  return Promise.all([
    getSetting(host, 'network', 'restart_game_year'),
    getSetting(host, 'economy', 'day_length_factor')
  ]).then(([endYear, dayLength]) => {
    return {
      endYear: parseInt(endYear.value || '0') || 0,
      dayLengthFactor: parseInt(dayLength.value || '1') || 1,
      appliedNextGameSettings: false
    }
  })
}

const isRotationServer = (event: OpenTTDEvent<unknown>) => event.serverName === 'reddit3' || event.serverName === 'testserver1'

const welcomeHandler = (event: OpenTTDEvent<GameInfo>) => {
  if (!isRotationServer(event))  {
    return
  }
  return getEndGameInfo(event.serverHost)
    .then(serverEndGameInfo => {
      endGameInfo[event.serverName] = serverEndGameInfo
    })
    .catch(err => {
      console.log(`[server3-rotation:welcomeHandler] Error retrieving ${event.serverName} game setting: ${err.message}`)
    })
}

export const init = () => {
  onWelcome(welcomeHandler)
  onDate((event: OpenTTDEvent<number>) => {
    if (!isRotationServer(event))  {
      return
    }
    
    const endGamePromise = !!endGameInfo[event.serverName] 
      ? Promise.resolve(endGameInfo[event.serverName] ) 
      : getEndGameInfo(event.serverHost)
          .then(serverEndGameInfo => {
            endGameInfo[event.serverName] = serverEndGameInfo
          })
    return endGamePromise.then((endGame) => {
      if (endGame && endGame.endYear > 0) {
        const timeLeft = getGameDurationMs(event.data, endGame.endYear, endGame.dayLengthFactor)
        if (!endGame.appliedNextGameSettings && timeLeft < MINS_TO_APPLY * 1000 * 60){
          return getNextGameSettings(event.serverHost)
            .then(rotation => {
              return rotation.settings.reduce((promise, setting) => 
                promise.then(() => setNewGameSetting(event.serverHost, setting.section, setting.setting, setting.value))
              , Promise.resolve({}))
              .then(() => {
                sendBroadcastMessage(event.serverHost, `NOTICE: Next game will be ${rotation.name}`)
                endGame.appliedNextGameSettings = true
              })
            })
            .catch(err => {
              console.log(`Error handling ${event.serverName} game setting update: ${err.message}`)
            })
        }
      }
    })
    .catch(err => {
      console.log(`[server3-rotation:onDate]  Error retrieving ${event.serverName} game setting: ${err.message}`)
      endGameInfo[event.serverName] = {
        endYear: 0, // kind of a null value.
        dayLengthFactor: 1,
        appliedNextGameSettings: false
      }
    })
  })
}