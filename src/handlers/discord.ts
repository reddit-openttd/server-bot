import { onPrivateChat, onChat }  from '../services/rabbitmq'
import { sendPrivateMessage } from '../services/restmin'
import { Chat, OpenTTDEvent } from '../types/OpenTTDEvent'
import {curry} from 'ramda'

const discordLink = 'https://discord.gg/openttd'

const handler = (event: OpenTTDEvent<Chat>) => {
  const chatDetail = event.data
  const command = (chatDetail.message || '').toLowerCase()
  if (command !== '!discord') { return }

  const send = curry(sendPrivateMessage)(event.serverHost, chatDetail.clientId)
  
  send(`Please join us on discord here ${discordLink}`)
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}