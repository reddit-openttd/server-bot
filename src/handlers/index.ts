
// import {publish} from '../services/rabbitmq'
// import {OpenTTDEventType} from '../types/OpenTTDEvent'

// const publishEvent = (host: string) => (event: OpenTTDEventType, data: any) => publish({
//   event,
//   host, 
//   data
// })

// export const init = () => {
// }
import * as help from './help'
import * as reset from './reset'
import * as welcome from './welcome'
import * as end from './end'
import * as endNotification from './end-notification'
import * as discord from './discord'
import * as rules from './rules'
import * as goals from './goals'
import * as server3Rotation from './server3-rotation'
import * as requirePassword from './require-password'

export const init = () => {
  help.init()
  welcome.init()
  reset.init()
  end.init()
  discord.init()
  rules.init()
  endNotification.init()
  server3Rotation.init(),
  requirePassword.init(),
  goals.init()
}