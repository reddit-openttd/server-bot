import { onDate, onWelcome }  from '../services/rabbitmq'
import { getSetting, sendBroadcastMessage } from '../services/restmin'
import { OpenTTDEvent } from '../types/OpenTTDEvent'
import { GameInfo } from '../types/GameInfo'
import { getGameDurationMs} from '../helper/date'
import humanizeDuration from 'humanize-duration'

const MINS_TO_FIRST_NOTIFY = 5
const SECONDS_TO_LAST_NOTIFY = 15

type EndGameInfo = {
  endYear: number,
  dayLengthFactor: number,
  hitFiveMinuteWarning: boolean
  hitLastWarning: boolean
}

let endGameInfo: Record<string, EndGameInfo> = {}

const welcomeHandler = (event: OpenTTDEvent<GameInfo>) => {
  return Promise.all([
    getSetting(event.serverHost, 'network', 'restart_game_year'),
    getSetting(event.serverHost, 'economy', 'day_length_factor')
  ]).then(([endYear, dayLength]) => {
    endGameInfo[event.serverName] = {
      endYear: parseInt(endYear.value || '0') || 0,
      dayLengthFactor: parseInt(dayLength.value || '1') || 1,
      hitFiveMinuteWarning: false,
      hitLastWarning: false
    }
  })
}

export const init = () => {
  onWelcome(welcomeHandler)
  onDate((event: OpenTTDEvent<number>) => {
    const endGame = endGameInfo[event.serverName]
    if (endGame && endGame.endYear > 0) {
      const timeLeft = getGameDurationMs(event.data, endGame.endYear, endGame.dayLengthFactor)
      if (!endGame.hitFiveMinuteWarning && timeLeft < MINS_TO_FIRST_NOTIFY * 1000 * 60){
        sendBroadcastMessage(event.serverHost, `NOTICE: This game will end in ${humanizeDuration(timeLeft, { round: true })}`)
        endGame.hitFiveMinuteWarning = true
      }
      if (!endGame.hitLastWarning && timeLeft < SECONDS_TO_LAST_NOTIFY * 1000) {
        sendBroadcastMessage(event.serverHost, `NOTICE: About to restart in a few seconds..`)
        endGame.hitLastWarning = true
      }
    }
  })
}