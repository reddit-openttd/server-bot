import { onPrivateChat, onChat }  from '../services/rabbitmq'
import { getGameInfo, getSetting, sendPrivateMessage, sendRcon } from '../services/restmin'
import { Chat, HttpServerHost, OpenTTDEvent } from '../types/OpenTTDEvent'
import { getGameDurationMs} from '../helper/date'
import humanizeDuration from 'humanize-duration'

const handler = (event: OpenTTDEvent<Chat>) => {
  const chatDetail = event.data
  if (chatDetail.message && chatDetail.message.toLowerCase() !== '!end') { return }

  return Promise.all([
    getGameInfo(event.serverHost),
    getSetting(event.serverHost, 'network', 'restart_game_year'),
    getSetting(event.serverHost, 'economy', 'day_length_factor'),
  ]).then(([gameInfo, endYear, day_length]) => {
    const dayLength = parseInt(day_length.value || '1') || 1
    const restartGameYear = parseInt(endYear.value || '0') || 0
  
    if (!restartGameYear) {
      return sendPrivateMessage(event.serverHost, event.data.clientId, 'End year not set.')
    }
    if (!gameInfo.date) {
      return sendPrivateMessage(event.serverHost, event.data.clientId, 'Date not available just yet.')
    }
    const gameDuration = getGameDurationMs(gameInfo.date, restartGameYear, dayLength)

    sendPrivateMessage(event.serverHost, chatDetail.clientId, `The game will end in ${humanizeDuration(gameDuration, { round: true })}`)
  })
}

export const init = () => {
  onChat(handler)
  onPrivateChat(handler)
}