import dotenv from 'dotenv'
import * as rabbitmq from './services/rabbitmq'
import * as handlers from './handlers'

import sanityCheck from './sanity'

dotenv.config()

sanityCheck()

rabbitmq.init()
handlers.init()
