export type MapInfo = {
  name: string
  seed: number
  landscape: number
  startdate: number
  mapheight: number
  mapwidth: number
}
export type GameInfo = {
  dedicated: 0 | 1
  map: MapInfo
  name: string
  version: string
  date?: number
}