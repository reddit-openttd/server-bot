export type ModeratorEventType = 'chat' | 'private-chat'

// "sexual": true,
// "hate": false,
// "violence": false,
// "self-harm": false,
// "sexual/minors": false,
// "hate/threatening": false,
// "violence/graphic": false

export type HttpServerHost = {
  url: string
}

export type ClientEvent = {
  clientId: number
}

export type ModeratedChat = ClientEvent & {
  flags: string[]
  message: string,
  money: number
}


export type ModeratorEvent<T> = {
  serverHost: HttpServerHost,
  serverName: string,
  event: ModeratorEventType,
  data: T
}