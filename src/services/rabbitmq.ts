import amqp from 'amqplib'
import events from 'events'
import { GameInfo } from '../types/GameInfo';
import type {Chat, ClientEvent, ConsoleEvent, OpenTTDEvent} from '../types/OpenTTDEvent'
import type {ModeratedChat, ModeratorEvent} from '../types/ModeratorEvent'

// if the connection is closed or fails to be established at all, we will reconnect
let amqpConn:amqp.Connection;
const EVENTS_EXCHANGE = 'openttd-events'

export const openttdEmitter = new events.EventEmitter();

const start = async (): Promise<void> => {
  try {
    const host = process.env.RABBIT_MQ_HOST + (process.env.RABBIT_MQ_PORT ? `:${process.env.RABBIT_MQ_PORT}` : '')
    amqpConn = await amqp.connect('amqp://' + host + "?heartbeat=60")
  }
  catch(err: any) {
    console.error("[AMQP]", err.message)
    return new Promise((resolve) => setTimeout(() => resolve(start()), 1000))
  }
  amqpConn.on("error", function(err) {
    if (err.message !== "Connection closing") {
      console.error("[AMQP] conn error", err.message)
    }
  })
  amqpConn.on("close", function() {
    console.error("[AMQP] reconnecting")
    return new Promise((resolve) => setTimeout(() => resolve(start()), 1000))
  })
  return onConnected()
}

const onConnected = () => {
  return startConsumer()
}

const startConsumer = () => {
  return amqpConn.createChannel()
    .then(channel => {
      channel.on("error", function(err) {
        console.error("[AMQP] channel error", err.message);
      })
      channel.on("close", function() {
        console.log("[AMQP] channel closed");
      })
      return channel.assertQueue('', {exclusive: true})
        .then(q => {
          channel.assertExchange(EVENTS_EXCHANGE, 'fanout', {durable: false})
          channel.bindQueue(q.queue, EVENTS_EXCHANGE, '')
          channel.consume(q.queue, (msg) => {
            if (msg !== null) {
              channel.ack(msg);
              let obj;
              try {
                obj = JSON.parse(msg.content.toString())
              }
              catch(err) {
                console.log('Unable to parse event: ' + msg.content)
                return
              }
              try {
                openttdEmitter.emit(obj.event, obj)
              } catch(err: any) {
                console.log('Error relaying event: ' + err.message)
              }
              
            } else {
              console.log('Consumer cancelled by server');
            }
          })
        })
    })
}

export const onJoin = (handler: (event: OpenTTDEvent<ClientEvent>) => void) => {
  openttdEmitter.on('clientjoin', handler)
}

export const onQuit = (handler: (event: OpenTTDEvent<ClientEvent>) => void) => {
  openttdEmitter.on('clientquit', handler)
}

export const onModeratorChatPrivate = (handler: (event: ModeratorEvent<ModeratedChat>) => void) => {
  openttdEmitter.on('moderator:chat', handler)
}

export const onModeratorChat = (handler: (event: ModeratorEvent<ModeratedChat>) => void) => {
  openttdEmitter.on('moderator:chat-private', handler)
}

export const onChat = (handler: (event: OpenTTDEvent<Chat>) => void) => {
  openttdEmitter.on('chat', handler)
}

export const onPrivateChat = (handler: (event: OpenTTDEvent<Chat>) => void) => {
  openttdEmitter.on('chat-private', handler)
}
export const onConsole = (handler: (event: OpenTTDEvent<ConsoleEvent>) => void) => {
  openttdEmitter.on('console', handler)
}
export const onNewGame = (handler: (event: OpenTTDEvent<null>) => void) => {
  openttdEmitter.on('newgame', handler)
}
export const onWelcome = (handler: (event: OpenTTDEvent<GameInfo>) => void) => {
  openttdEmitter.on('welcome', handler)
}
export const onShutdown = (handler: (event: OpenTTDEvent<null>) => void) => {
  openttdEmitter.on('shutdown', handler)
}
export const onDate = (handler: (event: OpenTTDEvent<number>) => void) => {
  openttdEmitter.on('date', handler)
}

export const init = () => {
  start()
}